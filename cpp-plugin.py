'''
The original code from this plugin is created by Akshay Khadse (https://gist.github.com/akshaykhadse).
I've made some heavy changes to make the code better fit to my classes.
'''
import subprocess
import os
import base64
import requests
import io
import re
import matplotlib.pyplot as plt
from IPython.core.magic import Magics, cell_magic, magics_class
from IPython.core.magic_arguments import argument, magic_arguments, parse_argstring
from pygments import highlight
from pygments.lexers import CppLexer
from pygments.formatters import HtmlFormatter
from IPython.display import display, HTML
from PIL import Image

def print_out(out: str):
    for l in out.split('\n'):
        print(l)

def displayHTML(html_code):
    display(HTML(html_code))

@magics_class
class CPP(Magics):

    @magic_arguments()
    @argument('-n', '--name', type=str, help='A file name that will be produced by the code of the cell.')
    @argument('-a', '--append', help='Should the cell\'s code be appended to same file.', action="store_true")
    @argument('-p', '--print', help='Print the cell\'s code.', action="store_true")
    @argument('-f', '--files', type=str, help='The list of files to concat and compile separated by a space.')
    @argument('-r', '--run', help='Run the last compiled program and display its output.', action="store_true")
    @argument('-m', '--mermaid', help='Run the last compiled program and display it as a Mermaid diagram.', action="store_true")
    @argument('-x', '--multi-mermaid', help='Run the last compiled program and display it as multiple Mermaid diagram.', action="store_true")
    @cell_magic
    def cpp(self, line='', cell=None):
        global style

        args = parse_argstring(self.cpp, line)
        
        #region Saving the cell as a file.
        if args.name != None:
            if args.append:
                mode = "a"
            else:
                mode = "w"
            with open(args.name, mode) as f:
                f.write(cell)
            if args.print:
                displayHTML(highlight(cell, CppLexer(), HtmlFormatter(full=True, nobackground=True, style='emacs')))
        #endregion

        #region Concatenating and compilation of the selected files.
        if args.files != None:
            input_filenames = args.files.split(',')
            try:
                with open('__output_program.cpp', 'w') as outfile:
                    for filename in input_filenames:
                        with open(filename) as input_code:
                            for line in input_code:
                                outfile.write(line)
                res = subprocess.check_output(["g++", "-std=c++20", "-o", "__output_program",  "__output_program.cpp"], stderr=subprocess.STDOUT)
                print_out(res.decode("utf8"))
            except subprocess.CalledProcessError as e:
                print_out(e.output.decode("utf8"))
        #endregion

        #region Running the program and printing its output.
        if args.run:
            try:
                res = subprocess.check_output(["./__output_program"], stderr=subprocess.STDOUT)
                print_out(res.decode("utf8"))
            except subprocess.CalledProcessError as e:
                print_out(e.output.decode("utf8"))
        #endregion.

        #region Running the program and sending its output to Mermaid.
        if args.mermaid:
            try:
                graph_bytes = subprocess.check_output(["./__output_program"], stderr=subprocess.STDOUT)
                base64_bytes = base64.b64encode(graph_bytes)
                base64_string = base64_bytes.decode("ascii")
                img = Image.open(io.BytesIO(requests.get('https://mermaid.ink/img/' + base64_string).content))
                plt.axis('off')
                plt.imshow(img)
            except subprocess.CalledProcessError as e:
                print_out(e.output.decode("utf8"))
            except Image.UnidentifiedImageError as e:
                print("Incorrect Mermaid graph:")
                print_out(graph_bytes.decode("utf8"))
        #endregion.

        #region Running the program and sending its output to Mermaid.
        if args.multi_mermaid:
            try:
                bytes = subprocess.check_output(["./__output_program"], stderr=subprocess.STDOUT)
                output = bytes.decode("utf8")
                graphs = re.findall(r'GRAPH_BEGIN\{\{\{.*?\}\}\}GRAPH_END', output, re.MULTILINE | re.DOTALL)
                for graph in graphs:
                    output = output.replace(graph, "")
                output = output.replace('\n\n','\n')
                for graph in graphs:
                  graph = graph[14:-12]
                  base64_bytes = base64.b64encode(graph.encode())
                  base64_string = base64_bytes.decode("ascii")
                  img = Image.open(io.BytesIO(requests.get('https://mermaid.ink/img/' + base64_string).content))
                  plt.figure()
                  plt.axis('off')
                  plt.imshow(img)
                print_out(output)
            except subprocess.CalledProcessError as e:
                output = e.output.decode("utf8")
                graphs = re.findall(r'GRAPH_BEGIN\{\{\{.*?\}\}\}GRAPH_END', output, re.MULTILINE | re.DOTALL)
                for graph in graphs:
                    output = output.replace(graph, "")
                output = output.replace('\n\n','\n')
                for graph in graphs:
                  graph = graph[14:-12]
                  base64_bytes = base64.b64encode(graph.encode())
                  base64_string = base64_bytes.decode("ascii")
                  img = Image.open(io.BytesIO(requests.get('https://mermaid.ink/img/' + base64_string).content))
                  plt.figure()
                  plt.axis('off')
                  plt.imshow(img)
                print_out(output)
            except Image.UnidentifiedImageError as e:
                print("Incorrect Mermaid graph:")
                print_out(graph_bytes.decode("utf8"))
        #endregion.

def load_ipython_extension(ip):
    os.system('pip install pygments ipywidgets')
    plugin = CPP(ip)
    ip.register_magics(plugin)
